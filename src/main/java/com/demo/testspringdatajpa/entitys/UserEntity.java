package com.demo.testspringdatajpa.entitys;

import lombok.Data;

import javax.persistence.*;

@Table
@Entity
@Data
public class UserEntity {

    @Id
    private Integer id;

    private String name;


}
