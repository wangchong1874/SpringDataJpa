package com.demo.testspringdatajpa.repository;

import com.demo.testspringdatajpa.entitys.UserEntity;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserEntityRepository extends Repository<UserEntity,String> {

    @Query("from UserEntity where id=:id")
    UserEntity findById(Integer id);

    List<UserEntity> findByNameAndIdOrderByIdDesc(String name,int id);

    List<UserEntity> readBy();

    int countBy();
}
