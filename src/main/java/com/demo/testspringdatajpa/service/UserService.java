package com.demo.testspringdatajpa.service;

import com.demo.testspringdatajpa.entitys.UserEntity;
import com.demo.testspringdatajpa.repository.UserEntityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService {
    @Resource
    private UserEntityRepository repository;

    public UserEntity findById(Integer id) {
        return repository.findById(id);
    }


}
