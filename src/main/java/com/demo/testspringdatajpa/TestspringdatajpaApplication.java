package com.demo.testspringdatajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
public class TestspringdatajpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestspringdatajpaApplication.class, args);
    }

}
