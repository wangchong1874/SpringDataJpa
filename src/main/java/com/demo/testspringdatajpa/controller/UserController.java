package com.demo.testspringdatajpa.controller;

import com.demo.testspringdatajpa.entitys.UserEntity;
import com.demo.testspringdatajpa.repository.UserEntityRepository;
import com.demo.testspringdatajpa.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private UserEntityRepository userEntityRepository;

    @GetMapping(value = "findById")
    public UserEntity findById(Integer id) {
        List<UserEntity> name = userEntityRepository.findByNameAndIdOrderByIdDesc("name", 1);
        System.out.println(name);
        return userService.findById(id);
    }

    @GetMapping(value = "readAll")
    public List<UserEntity> readAll() {
        return userEntityRepository.readBy();
    }

    @GetMapping(value = "count")
    public int count() {
        return userEntityRepository.countBy();
    }

}
